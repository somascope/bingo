const squares = document.querySelectorAll(".square");

let events = null;

// Blitz
let eventsGame1 = [
  "1st & goal",
  "2nd down",
  "3rd down",
  "3rd down conversion",
  "Third 1st down on a drive",
  "10 point lead",
  "10 yard penalty",
  "10+ yard run",
  "15 yard penalty",
  "20+ yard pass completion",
  "20+ yard run",
  "20+ yard punt return",
  "30+ yard pass completion",
  "5 yard penalty",
  "extra point",
  "fair catch",
  "field goal",
  "fumble",
  "incomplete pass",
  "interception",
  "jumped over player",
  "loss of yards",
  "missed field goal",
  "officials' huddle",
  "player pushed out of bounds",
  "punt",
  "QB rushes for yards",
  "QB sack",
  "team crosses midfield",
  "team enters red zone",
  "touchback",
  "touchdown (passing)",
  "touchdown (rushing)",
  "touchdown dance",
  "phone tv ad",
  "beer tv ad",
  "pizza tv ad",
  "tv show spot",
  "SUV/truck tv ad",
];

// Long haul
let eventsGame2 = [
  "1st & goal",
  "3rd down conversion",
  "4th down conversion",
  "50-yard pass play",
  "50-yard return",
  "blocked field goal",
  "blocked pass",
  "blocked punt",
  "coach’s timeout",
  "delay of game",
  "down and 15+ yards",
  "extra point",
  "face mask",
  "failed challenge",
  "fair catch",
  "false start",
  "field goal",
  "fumble",
  "Hail Mary pass",
  "helmet-to-helmet",
  "holding",
  "horse-collar tackle",
  "incomplete pass",
  "injured player",
  "inter&shy;ception",
  "measure for 1st down",
  "missed field goal",
  "offside",
  "personal foul",
  "pass inter&shy;ference",
  "punt",
  "punt returned for TD",
  "QB runs for 1st down",
  "QB sack",
  "roughing the passer",
  "rushing, loss of yards",
  "score off a turnover",
  "touchdown (passing)",
  "touchdown (rushing)",
  "safety",
  "succes&shy;sful challenge",
  "touch back",
  "two-point conversion",
];

// Commercials 'n' crap
let eventsGame3 = [
  "beer tv ad",
  "big tech tv ad",
  "coach or player in tv ad",
  "coach yelling at referee",
  "credit card tv ad",
  "extra point",
  "field goal",
  "fast food tv ad",
  "incomplete pass",
  "instant replay",
  "insurance tv ad",
  "jewelry tv ad",
  "NFL tv ad",
  "mattress tv ad",
  "movie trailer",
  "overhead shot of stadium",
  "pharmaceutical tv ad",
  "phone tv ad",
  "pizza tv ad",
  "retail store tv ad",
  "snack/chips tv ad",
  "soda tv ad",
  "sports event tv ad",
  "SUV/truck tv ad",
  "shaving tv ad",
  "team owner",
  "touchdown",
  "touchdown dance",
  "tv show spot",
  "video game tv ad",
];

// 4th quarter madness
let eventsGame4 = [
  "2-minute warning",
  "20+ yard run",
  "3rd down conversion",
  "30+ yard pass completion",
  "4th down, going for it",
  "blitz",
  "challenge",
  "extra point",
  "fair catch",
  "false start",
  "field goal",
  "fumble",
  "Hail Mary pass",
  "incomplete pass",
  "injured player",
  "interception",
  "jump over player",
  "lateral pass",
  "offside",
  "onside kick",
  "personal foul",
  "punt",
  "QB rushes for 1st down",
  "QB sack",
  "QB spike",
  "substitute QB",
  "touchdown",
  "touchdown dance",
  "two-point conversion",
  "roughing the passer",
  "team enters red zone",
  "team's first timeout",
  "team's second timeout",
  "team's third timeout",
  "throw-away pass",
];

// Flag on the field
let eventsGame5 = [
  "12 men on the field",
  "2 flags on a play",
  "block in back",
  "chop block",
  "clipping",
  "delay of game",
  "encroachment",
  "ejection",
  "face mask",
  "false start",
  "helmet to helmet",
  "holding",
  "horse collar tackle",
  "illegal contact",
  "illegal forward pass",
  "illegal formation",
  "illegal use of hands",
  "intentional grounding",
  "ineligible man downfield",
  "neutral zone infraction",
  "offside",
  "pass interference",
  "penalty declined",
  "personal foul",
  "roughing the kicker",
  "roughing the passer",
  "safety",
  "targeting",
  "tripping",
];

// Freaky Fast
let eventsGame6 = [
  "1st down",
  "1st down",
  "2nd down",
  "2nd down",
  "3rd down",
  "3rd down",
  "4th down",
  "4th down",
  "3rd down conversion",
  "3rd down conversion",
  "1-9 yard run",
  "1-9 yard run",
  "10+ yard run",
  "10+ yard run",
  "1-9 yard pass completion",
  "1-9 yard pass completion",
  "10+ yard pass completion",
  "10+ yard pass completion",
  "extra point",
  "field goal",
  "missed kicking point",
  "fumble",
  "incomplete pass",
  "incomplete pass",
  "loss of yards",
  "loss of yards",
  "punt",
  "punt",
  "QB sack",
  "team crosses midfield",
  "team enters red zone",
  "touchdown",
];

const games = {
  game1: {
    name: `Blitz`,
  },
  game2: {
    name: `Long Haul`,
  },
  game3: {
    name: `Commercials 'n' crap`,
  },
  game4: {
    name: `4th quarter madness`,
  },
  game5: {
    name: `Flag on the field`,
  },
  game6: {
    name: `Freaky Fast`,
  },
};

const colors = [
  {
    team: "Default Colors",
    dark: "#006db6",
    darkActive: "#268acc",
    light: "#ffc62f",
    checkmark: "#ffc62f",
    logo: "NFL.png",
  },
  {
    team: "Arizona Cardinals",
    dark: "#97233F",
    darkActive: "#A43E55",
    light: "#FFB612",
    checkmark: "#FFB612",
    logo: "AZ.png",
  },
  {
    team: "Atlanta Falcons",
    dark: "#a71930",
    darkActive: "#B63E52",
    light: "#FFB612",
    checkmark: "#FFB612",
    logo: "ATL.png",
  },
  {
    team: "Baltimore Ravens",
    dark: "#241773",
    darkActive: "#1E4B9B",
    light: "#9e7c0c",
    checkmark: "#9e7c0c",
    logo: "BAL.png",
  },
  {
    team: "Buffalo Bills",
    dark: "#00338d",
    darkActive: "#1E4C9C",
    light: "#c60c30",
    checkmark: "#c60c30",
    logo: "BUF.png",
  },
  {
    team: "Carolina Panthers",
    dark: "#0085CA",
    darkActive: "#2998D2",
    light: "#FFB612",
    checkmark: "#000000",
    logo: "CAR.png",
  },
  {
    team: "Chicago Bears",
    dark: "#0b162a",
    darkActive: "#323B4C",
    light: "#c83803",
    checkmark: "#c83803",
    logo: "CHI.png",
  },
  {
    team: "Cincinnati Bengals",
    dark: "#FB4F14",
    darkActive: "#FB6B3A",
    light: "#000000",
    checkmark: "#000000",
    logo: "CIN.png",
  },
  {
    team: "Cleveland Browns",
    dark: "#FF3C00",
    darkActive: "#FF5A29",
    light: "#000000",
    checkmark: "#000000",
    logo: "CLE.png",
  },
  {
    team: "Dallas Cowboys",
    dark: "#002244",
    darkActive: "#1E3E5C",
    light: "#ffc62f",
    checkmark: "#869397",
    logo: "DAL.png",
  },
  {
    team: "Detroit Lions",
    dark: "#0076b6",
    darkActive: "#1F88BF",
    light: "#ffc62f",
    checkmark: "#b0b7bc",
    logo: "DET.png",
  },
  {
    team: "Denver Broncos",
    dark: "#002244",
    darkActive: "#1E3E5C",
    light: "#fb4f14",
    checkmark: "#fb4f14",
    logo: "DEN.png",
  },
  {
    team: "Green Bay Packers",
    dark: "#203731",
    darkActive: "#3B4E4A",
    light: "#ffb612",
    checkmark: "#ffb612",
    logo: "GB.png",
  },
  {
    team: "Houston Texans",
    dark: "#03202f",
    darkActive: "#223C48",
    light: "#a71930",
    checkmark: "#a71930",
    logo: "HOU.png",
  },
  {
    team: "Indianapolis Colts",
    dark: "#002c5f",
    darkActive: "#1C4671",
    light: "#ffc62f",
    checkmark: "#ffc62f",
    logo: "IND.png",
  },
  {
    team: "Jacksonville Jaguars",
    dark: "#006778",
    darkActive: "#197c8d",
    light: "#9f792c",
    checkmark: "#9f792c",
    logo: "JAX.png",
  },
  {
    team: "Kansas City Chiefs",
    dark: "#e31837",
    darkActive: "#E73C57",
    light: "#ffb612",
    checkmark: "#ffb612",
    logo: "KC.png",
  },
  {
    team: "Las Vegas Raiders",
    dark: "#000000",
    darkActive: "#222222",
    light: "#ffc62f",
    checkmark: "#ffc62f",
    logo: "LV.png",
  },
  {
    team: "Los Angeles Chargers",
    dark: "#0080C6",
    darkActive: "#1E8FCB",
    light: "#ffb612",
    checkmark: "#ffb612",
    logo: "LAC.png",
  },
  {
    team: "Los Angeles Rams",
    dark: "#003594",
    darkActive: "#1E4EA2",
    light: "#FFD100",
    checkmark: "#FFD100",
    logo: "LA.png",
  },
  {
    team: "Miami Dolphins",
    dark: "#00838D",
    darkActive: "#1E9CA4",
    light: "#f26a24",
    checkmark: "#f26a24",
    logo: "MIA.png",
  },
  {
    team: "Minnesota Vikings",
    dark: "#4f2683",
    darkActive: "#644092",
    light: "#ffc62f",
    checkmark: "#ffc62f",
    logo: "MIN.png",
  },
  {
    team: "New England Patriots",
    dark: "#002244",
    darkActive: "#1E3E5C",
    light: "#c60c30",
    checkmark: "#c60c30",
    logo: "NE.png",
  },
  {
    team: "New Orleans Saints",
    dark: "#000000",
    darkActive: "#222222",
    light: "#d3bc8d",
    checkmark: "#d3bc8d",
    logo: "NO.png",
  },
  {
    team: "New York Giants",
    dark: "#0b2265",
    darkActive: "#293D78",
    light: "#a71930",
    checkmark: "#a71930",
    logo: "NYG.png",
  },
  {
    team: "New York Jets",
    dark: "#003f2d",
    darkActive: "#1F5646",
    light: "#ffc62f",
    checkmark: "#ffc62f",
    logo: "NYJ.png",
  },
  {
    team: "Philadelphia Eagles",
    dark: "#004c54",
    darkActive: "#1F6269",
    light: "#a5acaf",
    checkmark: "#a5acaf",
    logo: "PHI.png",
  },
  {
    team: "Pittsburgh Steelers",
    dark: "#000000",
    darkActive: "#292929",
    light: "#ffb612",
    checkmark: "#ffb612",
    logo: "PIT.png",
  },
  {
    team: "San Francisco 49ers",
    dark: "#aa0000",
    darkActive: "#B82929",
    light: "#b3995d",
    checkmark: "#b3995d",
    logo: "SF.png",
  },
  {
    team: "Seattle Seahawks",
    dark: "#002244",
    darkActive: "#1E3E5C",
    light: "#69be28",
    checkmark: "#69be28",
    logo: "SEA.png",
  },
  {
    team: "Tampa Bay Buccaneers",
    dark: "#A71930",
    darkActive: "#B2344A",
    light: "#B1BABF",
    checkmark: "#B1BABF",
    logo: "TB.png",
  },
  {
    team: "Tennessee Titans",
    dark: "#002244",
    darkActive: "#1E3E5C",
    light: "#4b92db",
    checkmark: "#C60C30",
    logo: "TEN.png",
  },
  {
    team: "Washington Football Team",
    dark: "#5A1414",
    darkActive: "#672525",
    light: "#ffb612",
    checkmark: "#ffb612",
    logo: "WAS.png",
  },
];
const version = "0.4.0";
const totalSpaces = 25; // 5x5 board
const freeSpace = 12; // middle space
const squaresCollection = document.querySelectorAll(".square");
const bingoList = [
  [0, 1, 2, 3, 4],
  [5, 6, 7, 8, 9],
  [10, 11, 12, 13, 14],
  [15, 16, 17, 18, 19],
  [20, 21, 22, 23, 24],
  [0, 5, 10, 15, 20],
  [1, 6, 11, 16, 21],
  [2, 7, 12, 17, 22],
  [3, 8, 13, 18, 23],
  [4, 9, 14, 19, 24],
  [0, 6, 12, 18, 24],
  [4, 8, 12, 16, 20],
];

let localStorageExists = false;
let rndmArray = [];
let rndmArrayLabels = [];
let createdTime = "";
let userSelections = [];
let userSelectionsClearSave = [];
let selectedTeam = "Default Colors";
let gameType = "";

// Audio
const audioBingo = document.getElementById("audio-bingo");

function setupBoard(userReset = false) {
  // Remove hidden class for elements
  document.querySelector("#main").classList.remove("hidden");
  document.querySelector(".footer").classList.remove("hidden");

  // Setup events
  if (gameType == 1) {
    events = eventsGame1;
  } else if (gameType == 2) {
    events = eventsGame2;
  } else if (gameType == 3) {
    events = eventsGame3;
  } else if (gameType == 4) {
    events = eventsGame4;
  } else if (gameType == 5) {
    events = eventsGame5;
  } else if (gameType == 6) {
    events = eventsGame6;
  }

  if (!localStorageExists || userReset) {
    rndmArray = uniqueSet(events.length - 1, totalSpaces);
  }

  rndmArrayLabels = [];

  squares.forEach(function (square, index) {
    square.addEventListener("click", handleSquare);
    let rndNum = rndmArray[index];
    const str = events[rndNum];
    if (index != freeSpace) {
      square.innerHTML = str;
      rndmArrayLabels.push(str);
    } else {
      // squares[freeSpace].innerText = "FREE";
    }
  });

  const timeStampEl = document.querySelector(".timestamp");
  if (!localStorageExists || userReset) {
    createdTime = getTimeNow();
  } else {
    createdTime = createdTime;
  }
  timeStampEl.innerText = `Card created: ${createdTime}`;

  // Set selections if localStorage exists
  if (localStorageExists) {
    presetSelections();
    if (!userReset) {
      checkForBingo();
    }
    if (!userReset) {
      handleColor(selectedTeam);
    }
  }

  const eventsName = document.querySelector("#modal-game-name");
  eventsName.innerHTML = getGameName();

  const eventsList = document.querySelector("#modal-events-list");
  if (eventsList.childNodes[0]) {
    eventsList.removeChild(eventsList.childNodes[0]);
  }
  eventsList.appendChild(makeUL(events));

  // Save data to LS
  localStorage.setItem("rndmArray", JSON.stringify(rndmArray));
  localStorage.setItem("createdTime", createdTime);
}

function setupHandlers() {
  const shuffleBtn = document.querySelector("#shuffle-btn");
  const newGameBtn = document.querySelector("#newGame-btn");
  const clearBtn = document.querySelector("#clear-btn");
  const select = document.querySelector("#teams-select");
  const listBtn = document.querySelector("#list-btn");

  const modalCloseBtns = document.querySelectorAll(".modal-1-close");
  for (const btn of modalCloseBtns) {
    btn.addEventListener("click", function (e) {
      MicroModal.close("modal-1");
    });
  }

  shuffleBtn.addEventListener("click", handleShuffle);
  newGameBtn.addEventListener("click", showNewGameModal);
  clearBtn.addEventListener("click", handleClear);
  listBtn.addEventListener("click", handleShowList);

  select.addEventListener("change", (e) => {
    const teamName = e.target.value;
    handleColor(teamName);
  });

  modalCloseBtns;

  const chooseGame1Btn = document.querySelector("#chooseGame1");
  const chooseGame2Btn = document.querySelector("#chooseGame2");
  const chooseGame3Btn = document.querySelector("#chooseGame3");
  const chooseGame4Btn = document.querySelector("#chooseGame4");
  const chooseGame5Btn = document.querySelector("#chooseGame5");
  const chooseGame6Btn = document.querySelector("#chooseGame6");

  chooseGame1Btn.addEventListener("click", handleChooseGame);
  chooseGame2Btn.addEventListener("click", handleChooseGame);
  chooseGame3Btn.addEventListener("click", handleChooseGame);
  chooseGame4Btn.addEventListener("click", handleChooseGame);
  chooseGame5Btn.addEventListener("click", handleChooseGame);
  chooseGame6Btn.addEventListener("click", handleChooseGame);

  if (localStorageExists) {
    const chooseGameResumeBtn = document.querySelector("#chooseGameResume");
    chooseGameResumeBtn.addEventListener("click", handleChooseGameResume);
    chooseGameResumeBtn.classList.remove("hidden");
  }
}

function init() {
  checkForLocalStorage();
  buildSelectOptions();
  setupHandlers();

  if (localStorageExists) {
    presetColorSelection(selectedTeam);
    handleColor(selectedTeam);
  } else {
    resetChoices();
  }

  const versionEl = document.querySelector("#version");
  versionEl.innerHTML = version;

  // Show setup window
  MicroModal.show("modal-setup");
}
init();

function handleChooseGameResume(e) {
  MicroModal.close("modal-setup");
  setupBoard(false);
  presetColorSelection(selectedTeam);
  showGameName();
}

function handleChooseGame(e) {
  const id = e.currentTarget.id.split("chooseGame")[1];
  gameType = id;
  localStorage.setItem("gameType", gameType);
  MicroModal.close("modal-setup");
  setupBoard(true);
  presetColorSelection(selectedTeam);
  handleColor(selectedTeam);
  handleClear(e);
  removeUndo();
  showGameName();
}

function showGameName() {
  document.querySelector("#game-name").innerHTML = getGameName();
}

function getGameName() {
  return games[`game${gameType}`].name;
}

function showNewGameModal(e) {
  MicroModal.show("modal-setup");
}

function checkForLocalStorage() {
  let _createdTime = localStorage.getItem("createdTime");
  if (_createdTime) {
    // LS exists - set variables to LS values
    localStorageExists = true;
    createdTime = _createdTime;

    let _rndmArray = JSON.parse(localStorage.getItem("rndmArray"));
    rndmArray = _rndmArray;

    let _userSelections = JSON.parse(localStorage.getItem("userSelections"));
    userSelections = _userSelections;

    let _selectedTeam = localStorage.getItem("selectedTeam");
    if (_selectedTeam) {
      selectedTeam = _selectedTeam;
    } else {
      selectedTeam = colors[0].team;
    }

    gameType = Number(localStorage.getItem("gameType"));
  } else {
    // LS doesn't exist - do nothing else
  }
}

function handleShowList(e) {
  MicroModal.show("modal-1");
}

function handleSquare(e) {
  const btn = e.target;
  const id = btn.dataset.id;
  const disabled = btn.classList.contains("disabled");
  if (!disabled) {
    removeUndo();
    // Do selections stuff
    btn.classList.toggle("active");
    const isActive = btn.classList.contains("active");
    updateSelections(id, isActive);

    // Check for bingo
    checkForBingo();
  }
}

function updateSelections(id, isActive) {
  userSelections[id] = isActive;

  // Save data to LS
  localStorage.setItem("userSelections", JSON.stringify(userSelections));
}

function presetSelections() {
  userSelections.forEach((selection, index) => {
    // const disabled = btn.classList.contains('disabled');
    if (selection) {
      const square = squaresCollection[index];
      square.classList.add("active");
    }
  });
}

// Bingo detection
function checkForBingo() {
  for (let b = 0; b < bingoList.length; b++) {
    const currentBingoArr = bingoList[b];
    let count = 0;
    for (let s = 0; s < currentBingoArr.length; s++) {
      if (userSelections[currentBingoArr[s]]) {
        count++;
      }
    }
    if (count === 5) {
      window.setTimeout(function () {
        showBingo(b);
      }, 50);
    } else if (count < 5) {
      noBingo(b);
    }
  }
}

function showBingo(bingoIndex) {
  const bingoSet = bingoList[bingoIndex];
  bingoSet.forEach((squareNum) => {
    const square = squaresCollection[squareNum];
    square.classList.add("bingo");
  });

  // audioBingo.play();
}

function noBingo(bingoIndex) {
  const bingoSet = bingoList[bingoIndex];
  bingoSet.forEach((squareNum) => {
    const square = squaresCollection[squareNum];
    square.classList.remove("bingo");
  });
}

// Button handlers
function handleShuffle(e) {
  setupBoard(true);
  handleClear(e);
  removeUndo();
}

function handleClear(e) {
  const label = String(e.target.innerHTML).toLocaleLowerCase();

  // Detect if any choices - only clear if > 0 choices.
  // Need to omit selection 12, which is the auto-selected freespace
  const a1 = userSelections.slice(0, freeSpace);
  const a2 = userSelections.slice(freeSpace + 1, userSelections.length);
  const userSelectionsNotFreeSpace = a1.concat(a2);
  if (!userSelectionsNotFreeSpace.includes(true) && label.includes("card")) {
    return;
  }

  if (label.includes("undo")) {
    doUndo();
    removeUndo();
    return;
  }

  prepareUndo(userSelections);

  squares.forEach(function (square, index) {
    const disabled = square.classList.contains("disabled");
    if (!disabled) {
      square.classList.remove("active");
    }
    square.classList.remove("bingo");
  });
  resetChoices();
}

function resetChoices() {
  userSelections = new Array(totalSpaces).fill(false);
  userSelections[freeSpace] = true;

  // Save data to LS
  localStorage.setItem("userSelections", JSON.stringify(userSelections));
}

function prepareUndo(choices) {
  userSelectionsClearSave = choices;

  const clearBtn = document.querySelector("#clear-btn");
  console.log("prepareUndo", clearBtn.innerHTML);
  clearBtn.innerHTML = "Undo Clear";
}

function doUndo() {
  userSelections = userSelectionsClearSave;
  presetSelections();
  checkForBingo();
}

function removeUndo() {
  const clearBtn = document.querySelector("#clear-btn");
  clearBtn.innerHTML = "Clear Card";
  userSelectionsClearSave = [];
}

function buildSelectOptions() {
  const select = document.querySelector("#teams-select");
  colors.forEach((team) => {
    const option = document.createElement("option");
    option.value = team.team;
    option.text = team.team;
    select.add(option);
  });
}

function handleColor(search) {
  selectedTeam = search;
  const result = colors.find((team) => team.team === search);
  document.documentElement.style.setProperty("--dark-color", result.dark);
  document.documentElement.style.setProperty(
    "--dark-color-active",
    result.darkActive
  );
  document.documentElement.style.setProperty("--light-color", result.light);
  document.documentElement.style.setProperty(
    "--checkmark-color",
    result.checkmark
  );

  // Update team logo
  const logoEl = document.querySelector(".logo");
  logoEl.setAttribute(
    "style",
    `background-image: url('img/logos/${result.logo}')`
  );

  // Update team QB photo (only for KC and TB)
  const playerEl = document.querySelector(".player-img");
  if (selectedTeam === "Kansas City Chiefs") {
    playerEl.setAttribute("src", "img/players/patrick-mahomes.png");
  } else if (selectedTeam === "Tampa Bay Buccaneers") {
    playerEl.setAttribute("src", "img/players/tom-brady.png");
  } else {
    playerEl.setAttribute("src", "");
  }

  // Save data to LS
  localStorage.setItem("selectedTeam", search);
}

function presetColorSelection(search) {
  const select = document.querySelector("#teams-select");
  if (search != "") {
    select.value = search;
  }
}

// Helpers
function uniqueSet(max, size) {
  var arr = [];
  while (arr.length < size) {
    var r = Math.floor(Math.random() * max) + 1;
    if (arr.indexOf(r) === -1) arr.push(r);
  }
  return arr;
}

function getTimeNow() {
  const event = new Date();
  const options = {
    weekday: "short",
    year: "numeric",
    month: "short",
    day: "numeric",
  };
  const date = event.toLocaleDateString("en-US", options);
  const time = event.toLocaleTimeString("en-US");
  return `${date}, ${time}`;
}

function makeUL(array) {
  // Create the list element:
  const list = document.createElement("ol");

  let str = "";
  for (let i = 0; i < array.length; i++) {
    // Create the list item:
    const item = document.createElement("li");

    // Set its contents:
    str = array[i];
    if (rndmArrayLabels.includes(array[i])) {
      str = `<div class="is-picked">${str}</div>`;
    }
    item.innerHTML = str;

    // Add it to the list:
    list.appendChild(item);
  }

  // Finally, return the constructed list:
  return list;
}
